package nl.han.aim.tourofheroes;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HeroControllerTest {

    @Mock
    private HeroRepository heroRepository;

    @InjectMocks
    private HeroController heroController;

    @Test
    void getHero() {
        Hero hero = new Hero(1L, "A");
        Optional<Hero> opt = Optional.of(hero);
        when(heroRepository.findById(1L)).thenReturn(opt);
        assertEquals( heroController.getHero(1L), hero);
    }

    @Test
    void whenSearchAppliedWithOutTermFindAllIsCalled() {
        Hero hero = new Hero(1L, "A");
        List<Hero> heroes = Arrays.asList(hero);
        when(heroRepository.findAll()).thenReturn(heroes);
        assertEquals( heroController.searchHeroes(null), heroes);
    }
}
